const capitalizar = (texto) => {
    if (typeof texto !== 'string') return ''
    return texto.charAt(0).toUpperCase() + texto.slice(1).toLocaleLowerCase()
}

module.exports = {
    capitalizar
}