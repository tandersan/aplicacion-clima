// Requires Paquetes
require('colors');

//Variables
const inquirer = require('inquirer');
const { capitalizar } = require('../fx/fxs');

//MAIN
const inquirerMenu = async() => {
    const opcionesMenu = [
        {
            type: 'list',
            name: 'opcionSel',
            message: '¿Qué desea ejecutar? ',
            choices: [
                {
                    value: '1',
                    name: `${'1.'.green} Buscar Ciudad`
                },
                {
                    value: '2',
                    name: `${'2.'.green} Historial`
                },
                {
                    value: '3',
                    name: `${'3.'.green} Salir\n`
                }
            ]
        }
    ]

    console.clear();
    console.log('========================='.green);
    console.log(' Menú Climas del Mundo: ');
    console.log('========================='.green);
    const { opcionSel } = await inquirer.prompt(opcionesMenu);
    return opcionSel;
}

const pausa = async() => {
    const opcionPausa = [
        {
            type: 'input',
            name: 'opcionPausa',
            message: `Presione ${'ENTER'.blue} para continuar ...`
        }
    ];

    await inquirer.prompt(opcionPausa);
}

const inputCiudad = async(message) => {
    const opcionInputCiudad = [
        {
            type: 'input',
            name: 'leeInputCiudad',
            message,
            validate(value) {
                if (value.length === 0) {
                    return `Favor inrgese el nombre de una ciudad`.red
                } else {
                    return true;
                }
            }
        }
    ];

    const { leeInputCiudad } = await inquirer.prompt(opcionInputCiudad);
    return leeInputCiudad;
}

const listaCiudades = async(ciudades = [], textoCiudadIngresado) => {
    let idFinalArregloCiudades = ciudades.length - 1;
    const choices = ciudades.map( (ciudad, idx) => {
        if (idFinalArregloCiudades !== idx) {
            return {
                value: ciudad.id,
                name: `${(++idx + '.').blue} [${ciudad.nombreLargo}]`
            }
        } else {
            return {
                value: ciudad.id,
                name: `${(++idx + '.').blue} [${ciudad.nombreLargo}]\n`
            }
        }
    });

    choices.unshift({
        value: '0',
        name: `${'0.'.blue} ${'Salir de Lista'.green}`
    });

    const opcionListaCiudadesSel = [
        {
            type: 'list',
            name: 'idCiudadClima',
            message: `Lista de ciudades con el nombre ${capitalizar(textoCiudadIngresado).green}\n  Seleccione una opción:`,
            choices
        }
    ];

    const { idCiudadClima } = await inquirer.prompt(opcionListaCiudadesSel);
    return idCiudadClima;
}

module.exports = {
    inquirerMenu,
    pausa,
    inputCiudad,
    listaCiudades
}