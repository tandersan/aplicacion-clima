// Requires Paquetes
require('colors');
require('dotenv').config();

//Variables
const { inquirerMenu, pausa, inputCiudad, listaCiudades } = require('./helpers/inquirer');
const Busquedas = require('./models/busquedas');
const { guardarDB, cargarDB } = require('./helpers/database');
let ciudadSelEnArr = [];

//MAIN
const main = async() => {
    let opcion = '';
    const busquedas = new Busquedas();
    busquedas.cargarHistorial(cargarDB());

    do {
        opcion = await inquirerMenu();

        switch(opcion) {
            case '1':
                // Mensaje de Inicio de la aplicación
                const ciudadABuscar = await inputCiudad('Ingrese el nombre de una Ciudad: ');

                // Buscando la ciudad que ha sido ingresada por consola
                const ciudadABuscarSinBlancos = ciudadABuscar.toString().trim();
                const ciudadesEncontradas = await busquedas.buscarCiudad(ciudadABuscarSinBlancos);

                // Seleccionando una ciudad de la lista encontrada
                const idCiudadSeleccionada = await listaCiudades(ciudadesEncontradas, ciudadABuscarSinBlancos);
                if (idCiudadSeleccionada === '0') continue;
                ciudadSelEnArr = ciudadesEncontradas.find(c => c.id === idCiudadSeleccionada);

                // Guardar en BBDD
                busquedas.agregarHistorial(ciudadSelEnArr.nombreCorto); // Guardamos la ciudad en el arreglo
                guardarDB(busquedas._listaCiudades); // Guardamos el arreglo en un JSON

                // Clima
                const climaCiudad = await busquedas.climaCiudad(ciudadSelEnArr.latitud, ciudadSelEnArr.longitud);

                // Mostrar formateada toda la información obtenida
                console.log(climaCiudad);
                break;

            case '2':
                console.log(busquedas._listaCiudades);
                break;
        }
        if (opcion !== '3') await pausa();
    } while (opcion !== '3')
}
//-------------------------------------------------------------------------------

main();