// Require
require('colors');

//Variables
const axios = require('axios');
const { capitalizar } = require('../fx/fxs');

//MAIN
class Busquedas {
    _listaCiudades = [];
    _mapboxApiParams = {};
    _openweatherApiParams = {};

    constructor() {
        //this._listaCiudades = ['Santiago', 'Madrid', 'Paris', 'Londres'];

        this._mapboxApiParams = {
            'access_token': process.env.MAPBOX_KEY || '',
            'limit': 3,
            'language': 'en'
        }

        this._openweatherApiParams = {
            'appid': process.env.OPENWEATHER_KEY || '',
            'units': 'metric',
            'lang': 'es'
        }
    }

    async buscarCiudad(nombreCiudad = '') {
        try {
            const instance = axios.create({
                baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${ nombreCiudad }.json`,
                params: this._mapboxApiParams
            });

            const respApi = await instance.get();

            return respApi.data.features.map( ciudad => ({
                id: ciudad.id,
                nombreCorto: ciudad.text,
                nombreLargo: ciudad.place_name,
                latitud: ciudad.center[1],
                longitud: ciudad.center[0],
                placeType: ciudad.place_type
            }));
        } catch (err) {
            console.log(err);
        }
    }

    async climaCiudad(lat, lon) {
        try {
            const instance = axios.create({
                baseURL: `https://api.openweathermap.org/data/2.5/weather`,
                params: {lat, lon, ...this._openweatherApiParams}
            });

            const respApi = await instance.get();
            const { cod, name, sys, weather, main } = respApi.data;

            return {
                httpCode: cod,
                nombreCiudad: name,
                codPais: sys.country,
                descClima: capitalizar(weather[0].description),
                tempActual: main.temp.toFixed(0) + '° C',
                tempMin: main.temp_min.toFixed(0)+ '° C',
                tempMax: main.temp_max.toFixed(0)+ '° C',
                humedad: main.humidity,
                presion: main.pressure
            };
        } catch (err) {
            console.log(err);
        }
    }

    async agregarHistorial(nombreCiudad = '') {
        if (!this._listaCiudades.includes(nombreCiudad.toLocaleLowerCase()))
            this._listaCiudades.unshift(nombreCiudad.toLocaleLowerCase());
    }


    async cargarHistorial(listaCiudadesDeBase = []) {
        if (listaCiudadesDeBase.length > 0) {
            this._listaCiudades = this._listaCiudades.concat(listaCiudadesDeBase)
        }
    }
}

module.exports = Busquedas;